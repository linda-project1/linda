<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'feature';
    protected $fillable = ['heading', 'icon', 'comment', 'core_feature_id'];
}
