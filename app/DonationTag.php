<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationTag extends Model
{
    protected $table = 'donation_tag';
    protected $guarded = [];
}
