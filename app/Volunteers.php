<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteers extends Model
{
    protected $table = 'volunteers';
    protected $fillable = ['name', 'position', 'img', 'volunteer_id'];
}
