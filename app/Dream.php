<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dream extends Model
{
    protected $table = 'dream';
    protected $with = ['dream_boxes'];
    protected $fillable = ['paragraph', 'title', ];

    /**
     * @return [type]
     */
    public function dream_boxes()
    {
    	return $this->hasMany(DreamBoxes::class);
    } 
}
