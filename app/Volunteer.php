<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $table = 'volunteer';
    protected $with = ['volunteers'];
    protected $fillable = ['title', 'paragraph'];

    /**
     * @return [type]
     */
    public function volunteers()
    {
    	return $this->hasMany(Volunteers::class, 'volunteer_id', 'id' );
    } 
}
