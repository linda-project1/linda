<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about_us';
    protected $with = ['dream', 'volunteer', 'core_features'];
    protected $fillable = ['faculty_id', 'about_title', 'banner', 'about_text', 'about_sub_title', 'about_wave_image', 'dream_id', 'core_feature_id', 'volunteer_id'];


    /**
     * @return [type]
     */
    public function dream()
    {
    	return $this->hasOne(Dream::class, 'id', 'dream_id');
    }

    /**
     * @return [type]
     */
    public function volunteer()
    {
    	return $this->hasOne(Volunteer::class, 'id', 'volunteer_id');
    }

    /**
     * @return [type]
     */
    public function core_features()
    {
    	return $this->hasOne(CoreFeatures::class, 'id', 'core_feature_id');
    }
}
