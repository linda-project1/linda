<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $rules = [] ;
     
 
        $rules = [
            'about_text'        =>  'required',
            'about_title'       =>  'required',
            'about_sub_title'   =>  'required'
        ];
        if(!$this->id){
            $rules ['banner'] ='required|mimes:jpg,jpeg,png|max:2048';
            $rules ['about_wave_image'] ='required|mimes:jpg,jpeg,png|max:2048';
        }
        else{
            $rules ['banner'] ='required';
            $rules ['about_wave_image'] ='required';
        }
        return $rules;
        
    }
}
