<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeatureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'core_features.paragraph' => 'required',
            'core_features.title' => 'required',
            'core_features.waving_video_link' => 'required|url',
            'core_features.feature.*.heading' => 'required',
            'core_features.feature.*.comment' => 'required',
            'core_features.feature.*.icon' => 'required',
        ];
    }

    public function messages()
    {
        return [
        'core_features.paragraph.required' => 'The content field is required.',
        'core_features.title.required' => 'The title field is required.',
        'core_features.waving_video_link.required' => 'The video link field is required.',
        'core_features.waving_video_link.url' => 'The video link is not valid required.',
        'core_features.feature.*.heading.required' => 'The title field is required.',
        'core_features.feature.*.comment.required' => 'The content field is required.',
        'core_features.feature.*.icon.required' => 'The icon field is required.',
        ];
    }
}
