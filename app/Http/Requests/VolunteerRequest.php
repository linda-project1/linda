<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VolunteerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $rules = [];
       
        $rules = [

            'volunteers_position.*' => 'required',
             'volunteers_name.*'    => 'required',

             'volunteer_title'      => 'required',
             'volunteer_paragraph'  => 'required'
        ];
        if(isset($this->volunteer_id)){
            $rules['files.*'] = 'required';
          }
          else{
              $rules['files.*'] = 'required|mimes:jpg,jpeg,png|max:2048';
          } 
        
        return $rules;
    }

    public function message()
    {
        return [
            'volunteer_title.required'        => 'The name title is required.',
            'volunteer_paragraph.required'    => 'The name content is required.',
        ];
    }
}
