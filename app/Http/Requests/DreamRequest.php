<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DreamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'dreams.paragraph'       =>  'required',
            'dreams.title'           =>  'required',
            'dreams.dream_boxes.*.box_title'    =>  'required',
            'dreams.dream_boxes.*.box_text'    =>  'required'
        ];
    }

    public function messages()
    {
        return [
            'dreams.paragraph.required'       =>  'The content fieled is required.',
            'dreams.title.required'           =>  'The title field is required.',
            'dreams.dream_boxes.*.box_title.required'    =>  'The content field is required.',
            'dreams.dream_boxes.*.box_text.required'    =>  'The title field is required.'
        ];
    }
}
