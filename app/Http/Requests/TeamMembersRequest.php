<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamMembersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $file = $this->file('files');
        $rules = [];
        $rules = [
            'team_members_name.*' => 'required',
            'team_members_position.*' => 'required'
        ];

        if($file != null){
            foreach($file as $key => $value){
                $rules ["files.".$key] ='required|mimes:jpg,jpeg,png|max:2048';
            }
        }
       
        return $rules;
    }
    public function messages()
    {
        return[
            'files.*.mimes'     => 'The image must be a file of type: jpg, jpeg, png.',
             'files.*.required' => 'The image field is required.' 
        ];
    }
}
