<?php

namespace App\Http\Controllers;

use App\Category;
use App\Donation;
use App\DonationTag;
use App\Image;
use App\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DonationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Donations Index Method
     * @return View
     */
    public function index() {
        $donations = Donation::with('category')->get();
        return view('admin.donations.index', compact('donations'));
    }

    /**
     * @return View
     */
    public function create() {
        $categories = Category::pluck('title', 'id');
        $tags = Tag::pluck('title', 'id');
        $title = 'Create Donation';

        return view('admin.donations.create', compact('categories', 'tags', 'title'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request) {
        try {
            $donation = new Donation();
            $this->saveDonation($request, $donation, 'save');
            return  redirect()->route('donations.index')
                ->with('success', 'Donations created successfully..!');
        } catch (\Exception $e){
            /** Log the error for debugging */
            logger($e->getMessage());
            return  redirect()->route('donations.index')
                ->with('error', 'Donation could not be created, something went wrong..!');
        }

    }

    /**
     * @param $id
     */
    public function view($id){
        dd('This will be linked to the front end page of donations detail, viewable as normal users would see.');
    }

    /**
     * Edit Donation details
     * @param $id
     * @return View
     */
    public function edit($id) {
        $donation = Donation::with(['image', 'tags'])->where('id', $id)->first();
        $categories = Category::pluck('title', 'id');
        $tags = Tag::pluck('title', 'id');
        $title = 'Edit Donation';
        return view('admin.donations.edit', compact('donation', 'categories', 'tags', 'title'));
    }

    /**
     * Update a donation record
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id) {
        try {
            $donation = Donation::where('id', $id)->first();
            $this->saveDonation($request, $donation, 'update');
            return  redirect()->route('donations.index')->with('success', 'Donations updated successfully..!');
        } catch (\Exception $e){
            /** Log the error for debugging */
            logger($e->getMessage());
            return  redirect()->route('donations.index')->with('error', 'Donation could not be updated, something went wrong..!');
        }

    }

    /**
     * Delete a donation and its resources
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id) {
        $donation = Donation::find($id);

        if($donation){
            $image = Image::where('id', $donation->image_id)->first();
            if($image){
                $this->deleteDonationImage($image);
            }
            $donation->delete();
            return redirect()->back()->with('success', 'Donation successfully deleted!');
        }
        return redirect()->back()->with('error', 'Donation could not be found!');
    }

    /**
     * Save donation model
     * @param $request
     * @param $donation
     * @param $action
     * @return RedirectResponse
     */
    public function saveDonation($request, $donation, $action) {
        $donation->title = $request->title;
        $donation->description = $request->description;
        $donation->category_id = $request->category;
        $donation->active = ($request->has('active')) ? 1 : 0;

        if($request->hasFile('image')){
            $path = $request->file('image')->store('images', 'public');
            $image = new Image();
            $image->path = $path;
            $image->save();

            /** Delete old image if updating and image available */
            if($action == 'update'){
                $oldImage = Image::where('id', $donation->image_id)->first();
                if ($oldImage){
                    $this->deleteDonationImage($oldImage);
                }
            }
            $donation->image_id = $image->id;
        } elseif ($action == 'save') {
            return redirect()->back()->with('error', 'Please select an image to upload...!');
        }

        $donation->save();

        // Save donation tags
        DonationTag::where('donation_id', $donation->id)->delete();
        if($request->has('tags')){
            foreach ($request->tags as $tagId){
                $tag = new DonationTag();
                $tag->donation_id = $donation->id;
                $tag->tag_id = $tagId;
                $tag->save();
            }
        }
    }

    /**
     * Delete donation image model and file
     * @param $image
     */
    public function deleteDonationImage($image){
        /** Check if image file exist and delete actual image file */
        if(file_exists(public_path('storage/'.$image->path))){
            unlink(public_path('storage/'.$image->path));
        }
        /** Delete image model */
        $image->delete();
    }
}
