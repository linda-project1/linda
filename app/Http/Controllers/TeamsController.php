<?php

namespace App\Http\Controllers;

use App\Http\Requests\bannerRequest;
use App\Http\Requests\TeamMembersRequest;
use App\TeamBanner;
use App\TeamMembers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use stdClass;

class TeamsController extends Controller
{
    
    public function index(){
        return view('admin.team');
    }

    public function getTeamInfo()
    {
        $data = new stdClass();
        
        $banner = TeamBanner::all();
        $data->team = new stdClass();
        $data->team = isset($banner[0]) ? $banner[0] : null;
        $members = TeamMembers::all(); 
        if(!empty($members) ){ 
            $data->team_members = $members;
            
            
        }
        $data = json_encode($data);
        return $data;
    }

    public function saveTeamBanner(bannerRequest  $request){
        if(isset($request->id))
            $oldImage = TeamBanner::where('id', $request->id)->value('banner'); 

        $img = $request->file('banner');
        $file_name = time().'_'.$img->getClientOriginalName();
        $banner_file_path = $img->storeAs('about-uploads/team/banner', $file_name, 'public');
        if(isset($request->id)){ 
            TeamBanner::where('id', $request->id)->update([
                'banner'            => '/storage/'.$banner_file_path
            ]);
        }
        else{
            TeamBanner::create([
                'banner'            => '/storage/'.$banner_file_path
            ]);
        }

        if(isset($request->id))
            Storage::disk('local')->delete($oldImage);

        return 'success';
    }
    public function getAllteamMembersId($request)
    {
        $allRequestIds = [];
        $names = $request->team_members_name;
        for($i = 0; $i < count($names); $i++ ){
            if(isset($request->team_members_position[$i.'_id']))
                $allRequestIds[] = $request->team_members_position[$i.'_id'];
        }
       return $allRequestIds;
    }

    public function deleleteTeamMembers($allRequestIds)
    {
        $oldTeamIds = TeamMembers::all();
        $idsToDelete = [];
        foreach($oldTeamIds as $oldTeamId){
            $found = false;
            foreach($allRequestIds as $request_id){
                if($request_id == $oldTeamId->id){
                    $found = true;
                    break;
                }
            }
            if(!$found)
                $idsToDelete[] = $oldTeamId->id;
        }
        TeamMembers::whereIn('id', $idsToDelete)->delete();
    }
    /**
     * @param Request $request
     * 
     * @return [type]
     */
    public function saveTeamMembers(TeamMembersRequest $request)
    {
        $allRequestIds = $this->getAllteamMembersId($request);
        $this->deleleteTeamMembers($allRequestIds);
        $files = $request->file('files');
        $names = $request->team_members_name;
        for($i = 0; $i < count($names); $i++ ){  
            $file_path = null;
            if(@is_array(getimagesize($files[$i]))){
                $file_name = time().'_'.$files[$i]->getClientOriginalName();
                $file_path = $files[$i]->storeAs('about-uploads/team/members', $file_name, 'public');
            }

            if(isset($request->team_members_position[$i.'_id'])){
                $id = $request->team_members_position[$i.'_id'];
                if($file_path != null){
                    $oldImage = TeamMembers::where('id', $id)->value('img'); 
                   TeamMembers::where('id', $id)->update([
                        'name'          => $names[$i], 
                        'posision'      => $request->team_members_position[$i],
                        'img'           => "/storage/".$file_path,
                    ]);
                    Storage::disk('local')->delete($oldImage);
                }
                else{
                    TeamMembers::where('id', $id)->update([
                        'name'          => $names[$i], 
                        'posision'      => $request->team_members_position[$i],
                    ]); 
                }
            }
            else{
                TeamMembers::create([
                    'name'          => $names[$i], 
                    'posision'      => $request->team_members_position[$i],
                    'img'           => "/storage/".$file_path,
                ]);
            }
        }
        return 'success';
    }
}
