<?php

namespace App\Http\Controllers;

use App\Category;
use App\Event;
use App\Image;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class EventController extends Controller
{
    private $event;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Event $event)
    {
        $this->middleware('auth')->except(['activeEvents', 'show']);
        $this->middleware('admin');
        $this->event = $event;
    }

    /**
     * Method to list all Admin events
     * @return View
     */
    public function index(){
        $events = $this->event->where('active', 1)->get();
        return view('admin.events.index', compact('events'));
    }

    /**
     * Admin Create Event
     * @return View
     */
    public function create() {
        $categories = Category::pluck('title', 'id');
        $title = 'Create Event';

        return view('admin.events.create', compact('categories',  'title'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request) {
        try {
            $event = new $this->event;
            $this->saveEvent($request, $event, 'save');
            return  redirect()->route('events.index')
                ->with('success', 'Donations created successfully..!');
        } catch (\Exception $e){
            /** Log the error for debugging */
            logger($e->getMessage());
            return  redirect()->route('events.index')
                ->with('error', 'Event could not be created, something went wrong..!');
        }

    }

    /**
     * @param $id
     */
    public function view($id){
        dd('This will be linked to the front end page of events detail, viewable as normal users would see.');
    }

    /**
     * Edit Event details
     * @param $id
     * @return View
     */
    public function edit($id) {
        $event = $this->event->with(['image'])->where('id', $id)->first();
        $categories = Category::pluck('title', 'id');
        $title = 'Edit Event';
        return view('admin.events.edit', compact('event', 'categories', 'title'));
    }

    /**
     * Update a event record
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id) {
        try {
            $event = $this->event->where('id', $id)->first();
            $this->saveEvent($request, $event, 'update');
            return  redirect()->route('events.index')->with('success', 'Event updated successfully..!');
        } catch (\Exception $e){
            /** Log the error for debugging */
            logger($e->getMessage());
            return  redirect()->route('events.index')->with('error', 'Event could not be updated, something went wrong..!');
        }

    }

    /**
     * Delete a event and its resources
     * @param $id
     * @return RedirectResponse
     */
    public function delete($id) {
        $event = $this->event->where('id', $id)->first();

        if($event){
            $image = Image::where('id', $event->image_id)->first();
            if($image){
                $this->deleteEventImage($image);
            }
            $event->delete();
            return redirect()->back()->with('success', 'Event successfully deleted!');
        }
        return redirect()->back()->with('error', 'Event could not be found!');
    }

    /**
     * Save event model
     * @param $request
     * @param $event
     * @param $action
     * @return RedirectResponse
     */
    public function saveEvent($request, $event, $action) {
        $event->title = $request->title;
        $event->description = $request->description;
        $event->category_id = $request->category;
        $event->organizer = $request->organizer;
        $event->location = $request->location;
        $event->user_id = Auth::id();
        $event->active = ($request->has('active')) ? 1 : 0;
        $event->start_time = Carbon::parse($request->start_time);
        $event->end_time = Carbon::parse($request->end_time);
        $event->contact_number = $request->contact_number;
        $event->contact_email = $request->contact_email;

        if($request->hasFile('image')){
            $path = $request->file('image')->store('images', 'public');
            $image = new Image();
            $image->path = $path;
            $image->save();

            /** Delete old image if updating and image available */
            if($action == 'update'){
                $oldImage = Image::where('id', $event->image_id)->first();
                if ($oldImage){
                    $this->deleteEventImage($oldImage);
                }
            }
            $event->image_id = $image->id;
        } elseif ($action == 'save') {
            return redirect()->back()->with('error', 'Please select an image to upload...!');
        }
        $event->save();
    }

    /**
     * Delete event image model and file
     * @param $image
     */
    public function deleteEventImage($image){
        /** Check if image file exist and delete actual image file */
        if(file_exists(public_path('storage/'.$image->path))){
            unlink(public_path('storage/'.$image->path));
        }
        /** Delete image model */
        $image->delete();
    }

    /**
     * Method to retrieve all active events to users
     */
    public function activeEvents(){

    }

    /**
     * Method to display a single event to users
     */
    public function show(){

    }
}
