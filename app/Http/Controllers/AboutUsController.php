<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use App\Volunteers;
use App\Volunteer;
use App\Http\Requests\VolunteerRequest;
use App\Http\Requests\FeatureRequest;
use App\Http\Requests\DreamRequest;
use App\CoreFeatures;
use App\Dream;
use App\DreamBoxes;
use App\Feature;
use App\Http\Requests\AboutRequest;
use Illuminate\Support\Facades\Storage;
use stdClass;

class AboutUsController extends Controller
{

    /**
     * @return [type]
     */
    public function index()
    {
        return view('about');
    }

    public function getView()
    {   
        $about = $this->getAbout();
        return view('admin.about')->with(compact('about'));
        
    }

    /**
     * @return [type]
     */
    public function getAbout()
    {
        $data = new stdClass();
        $dream = Dream::all();
        $data->dream = new stdClass();
        $data->dream = isset($dream[0]) ? $dream[0] : null;

        $core_features = CoreFeatures::all();
        $data->core_features = new stdClass();
        $data->core_features = isset($core_features[0]) ? $core_features[0] : null;

        $volunteer = Volunteer::all();
        $data->volunteer = new stdClass();
        $data->volunteer  = isset($volunteer[0] ) ? $volunteer[0] : null;

        $about_data = \DB::table('about_us')->select('*')->get();
        $data->about = new stdClass();
        $data->about = isset($about_data[0]) ? $about_data[0] : null;

        $about = json_encode($data);
        return $about;
    }

    /**
     * @param Request $request
     * 
     * @return [type]
     */
    public function saveVolunteers(VolunteerRequest $request)
    {   

        $volunteer =  Volunteer::create([
            'title'         =>  $request->volunteer_title,
            'paragraph'     =>  $request->volunteer_paragraph,
         ]);
    
        $this->createVolunteers($request,$volunteer );
        
        return 'success';
    }

    public function createVolunteers($request)
    {
        $volunteers_name = $request->volunteers_name;
        $data = Volunteer::latest('id')->first();
        $files = $request->file('files');
        
        for($i = 0; $i < count($volunteers_name); $i++ ){
            if(array_key_exists($i, $files)){ 
                 $file_name = time().'_'.$files[$i]->getClientOriginalName();
                $file_path = $files[$i]->storeAs('about-uploads/volunteers', $file_name, 'public');
                
                Volunteers::create([
                    'name'          => $volunteers_name[$i], 
                    'position'      => $request->volunteers_position[$i],
                    'img'           => '/storage/'.$file_path,
                    'volunteer_id'  => $data->id
                ]);
            }
           
    
        }
    }

    public function updateVolunteers(VolunteerRequest $request)
    {
        $files = $request->file('files');
        $volunteers_name = $request->volunteers_name;

        $volunteer =  Volunteer::where('id' , $request->volunteer_id)->update([
            'title'         =>  $request->volunteer_title,
            'paragraph'     =>  $request->volunteer_paragraph,
         ]);

         for($i = 0; $i < count($volunteers_name); $i++ ){  
            $file_path = null;
            if(@is_array(getimagesize($files[$i]))){
                $file_name = time().'_'.$files[$i]->getClientOriginalName();
                $file_path = $files[$i]->storeAs('about-uploads/volunteers', $file_name, 'public');
            }
           

            if(isset($request->volunteers_position[$i.'_id'])){
                $id = $request->volunteers_position[$i.'_id'];
                if($file_path != null){
                   Volunteers::where('id', $id)->update([
                        'name'          => $volunteers_name[$i], 
                        'position'      => $request->volunteers_position[$i],
                        'img'           => "/storage/".$file_path,
                    ]); 
                }
                else{
                    Volunteers::where('id', $id)->update([
                        'name'          => $volunteers_name[$i], 
                        'position'      => $request->volunteers_position[$i],
                    ]); 
                }
                
                
            }
            else
                $this->createVolunteers($request);
    
        }
        
        return 'success';
    }

    public function deleteFutures($oldFeatures, $core_features)
    {
        $idToDelete = [];
        foreach($oldFeatures as $value){
            $found = false;
            foreach($core_features as $feature){
                if(isset($feature['id']) && $value['id'] == $feature['id']){
                    $found = true;
                    break;
                }
            }
            if(!$found)
                $idToDelete[] = $value['id'];
        }

        Feature::whereIn('id', $idToDelete)->delete();
    }

    /**
     * @param FeatureRequest $request
     * 
     * @return [type]
     */
    public function saveFeatures(FeatureRequest $request)
    { 
        $core_features = $request->core_features;

        if (isset($core_features['id'])){

            $oldFeatures =  Feature::select('id')->where('core_feature_id', $core_features['id'])->get();

            CoreFeatures::where('id', $core_features['id'] )->update([
                'title'         =>  $core_features['title'],
                'paragraph'     =>  $core_features['paragraph'],
                'waving_video_link' =>  $core_features['waving_video_link']
            ]);
            
            $this->deleteFutures($oldFeatures, $core_features['feature']);

            foreach($core_features['feature'] as $key => $value){
                if(isset($value['id'])){
                    Feature::where('id', $value['id'] )->update([
                        'heading'       =>  $value['heading'],
                        'icon'          =>  $value['icon'],
                        'comment'       =>  $value['comment'],
                    ]);
                }
                else{
                    Feature::create([
                        'heading'       =>  $value['heading'],
                        'icon'          =>  $value['icon'],
                        'comment'       =>  $value['comment'],
                        'core_feature_id' => $core_features['id']
                    ]);
                }
            }
        }
        else{
             $feature = CoreFeatures::create([
                    'title'         =>  $core_features['title'],
                    'paragraph'     =>  $core_features['paragraph'],
                    'waving_video_link' =>  $core_features['waving_video_link']
                ]);
        
            foreach($core_features['feature'] as $key => $value){
                Feature::create([
                    'heading'       =>  $value['heading'],
                    'icon'          =>  $value['icon'],
                    'comment'       =>  $value['comment'],
                    'core_feature_id' => $feature->id
                ]);
            }
        }
       
        
        return 'success';
       
    }

    public function deleteDreamBoxes($oldDreamBoxes, $dreams)
    {
        $idToDelete = []; 
        foreach ($oldDreamBoxes as $key => $value){
            $found = false;
            foreach($dreams as $dream){
                if(isset($dream['id']) && $dream['id'] == $value['id']){
                    $found = true;
                    break;
                }
            }
            if(!$found)
                $idToDelete[] = $value['id'];
        }
        DreamBoxes::whereIn('id', $idToDelete)->Delete();
    }

    /**
     * @param DreamRequest $request
     * 
     * @return [type]
     */
    public function saveDreams(DreamRequest $request)
    { 
        $dreams = $request['dreams']; 
        
        if(isset($dreams['id'])){
            Dream::where('id', $dreams['id'])->update([
                'paragraph'         =>      $dreams['paragraph'],
                'title'             =>      $dreams['title']
            ]);
            
            $oldDreamBoxes = DreamBoxes::select('id')->where('dream_id', $dreams['id'])->get();
            $this->deleteDreamBoxes($oldDreamBoxes, $dreams['dream_boxes']);

            foreach($dreams['dream_boxes'] as $key => $value){
                if(isset($value['id'])){
                    DreamBoxes::where('id', $value['id'])->update([
                        'box_title'     =>  $value['box_title'],
                        'box_text'      =>  $value['box_text'],
                    ]); 
                }
                else{
                    DreamBoxes::create([
                        'box_title'     =>  $value['box_title'],
                        'box_text'      =>  $value['box_text'],
                        'dream_id'      =>  $dreams['id']
                    ]);
                }
            }
        }
        else{
                $dream = Dream::create([
                    'paragraph'         =>      $dreams['paragraph'],
                    'title'             =>      $dreams['title']
                ]);
                
                foreach($dreams['dream_boxes'] as $key => $value){
                    DreamBoxes::create([
                        'box_title'     =>  $value['box_title'],
                        'box_text'      =>  $value['box_text'],
                        'dream_id'      =>  $dream->id
                    ]);
                }
        }
       

        return 'success';

    }

    /**
     * @param AboutRequest $request
     * 
     * @return [type]
     */
    public function saveAbout(AboutRequest $request)
    { 
        if(isset($request['id'])){
            $this->updateAbout($request);
        }
        else{
            $deramId = Dream::latest('id')->first();
            $featureId = \DB::table('core_features')->select('id')->get(); 
            $volunteerId = Volunteer::latest('id')->first();

            $banner = $request->file('banner');
            $file_name = time().'_'.$banner->getClientOriginalName();
            $banner_file_path = $banner->storeAs('about-uploads/banner', $file_name, 'public');

            $image = $request->file('about_wave_image');
            $file_name = time().'_'.$image->getClientOriginalName();
            $image_file_path = $banner->storeAs('about-uploads/image', $file_name, 'public');

            About::create([
                'about_title'       =>  $request['about_title'],
                'banner'            =>  '/storage/'.$banner_file_path,
                'about_text'        =>  $request['about_text'],
                'about_sub_title'   =>  $request['about_sub_title'],
                'about_wave_image'  =>  '/storage/'.$image_file_path,
                'dream_id'          =>  $deramId->id,
                'core_feature_id'   =>  $featureId[0]->id,
                'volunteer_id'      =>  $volunteerId->id
            ]);
        }
        
        return 'success';
    }


    /**
     * @param mixed $request
     * 
     * @return [type]
     */
    public function updateAbout($request)
    {
        About::where('id', $request['id'])->update([
            'about_title'       =>  $request['about_title'],
            'about_text'        =>  $request['about_text'],
            'about_sub_title'   =>  $request['about_sub_title'],
        ]);
    }


    public function updateBanner(Request $request)
    {

        $oldImage = About::where('id', $request->id)->value('banner'); 
        $banner = $request->file('banner');
        $file_name = time().'_'.$banner->getClientOriginalName();
        $banner_file_path = $banner->storeAs('about-uploads/banner', $file_name, 'public');
        About::where('id', $request->id)->update([
            'banner'            => '/storage/'.$banner_file_path
        ]);

        Storage::disk('local')->delete($oldImage);

        return 'success';
    }

    /**
     * @param Request $request
     * 
     * @return [type]
     */
    public function updateImg(Request $request)
    {
        $oldImage = About::where('id', $request->id)->value('about_wave_image'); 
        $img = $request->file('img');
        $file_name = time().'_'.$img->getClientOriginalName();
        $banner_file_path = $img->storeAs('about-uploads/banner', $file_name, 'public');
        About::where('id', $request->id)->update([
            'about_wave_image'            => '/storage/'.$banner_file_path
        ]);

        Storage::disk('local')->delete($oldImage);

        return 'success';
    }
    /**
     * @return [type]
     */
    public function canEdit()
    { 
        $deramId = Dream::latest('id')->first();
        $featureId = CoreFeatures::latest('id')->first(); 
        $volunteerId = Volunteer::latest('id')->first();
        if (!empty($deramId) && !empty($featureId) && !empty($volunteerId)) {
            return json_encode(true);
        }
        else{
            return json_encode(false);
        }
    }
}
