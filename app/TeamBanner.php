<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamBanner extends Model
{
    protected $table = 'team_banner';
    protected $fillable = ['banner'];
}
