<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamBoxes extends Model
{
    protected $table = 'dream_boxes';
    protected $fillable = ['box_title', 'box_text', 'dream_id' ];
}
