<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMembers extends Model
{
    protected $table = 'team_members';
    protected $fillable = ['img', 'name', 'comment', 'posision'];
}
