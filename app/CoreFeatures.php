<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoreFeatures extends Model
{
    protected $table = 'core_features';
    protected $with = ['feature'];
    protected $fillable = ['paragraph', 'title', 'waving_video_link'];

    /**
     * @return [type]
     */
    public function feature()
    {
    	return $this->hasMany(Feature::class,'core_feature_id', 'id' );
    } 
}
