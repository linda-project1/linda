$(document).ready(function() {
    console.log('Admin.js Loaded!');

    $('.delete-donation').off('click');
    $('.delete-donation').on('click', function (e){

        console.log('.delete-donation invoked!');

        e.preventDefault();
        var id = $(this).attr('data-id');
        var url = '/admin/donations/delete/'+id;

        $('.btn-delete').attr('href', url);
        $('#confirmDelete').modal('toggle');
    });

    // Initialize custom select 2
    $('.custom-select2').select2();

    // Initialize Redactor plugin
    $R('.redactor', { styles: true });

    // DateTime picker
    if (window.jQuery().datetimepicker) {
        $('.datetimepicker').datetimepicker({
            // Formats
            // follow MomentJS docs: https://momentjs.com/docs/#/displaying/format/
            format: 'DD-MM-YYYY hh:mm A',

            // Your Icons
            // as Bootstrap 4 is not using Glyphicons anymore
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-check',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        });
    }

});
