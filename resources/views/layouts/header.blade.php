<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <!-- Ico Fonts CSS -->
        <link rel="stylesheet" href="assets/css/icofont.min.css">
        <!-- Mean Menu CSS -->
        <link rel="stylesheet" href="assets/css/meanmenu.css">
        <!-- Modal Video CSS -->
        <link rel="stylesheet" href="assets/css/modal-video.min.css">
        <!-- Flat Icons CSS -->
        <link rel="stylesheet" href="assets/fonts/flaticon.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <!-- Popup Image CSS -->
        <link rel="stylesheet" href="assets/css/lightbox.min.css">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
        <!-- Odometer CSS -->
        <link rel="stylesheet" href="assets/css/odometer.min.css">
        <!-- Style CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="assets/css/responsive.css">

        <title>Findo - Fundraising and Charity HTML Template</title>

        <link rel="icon" type="image/png" href="assets/img/favicon.png">
    </head>