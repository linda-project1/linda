<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- Ico Fonts CSS -->
    <link rel="stylesheet" href="assets/css/icofont.min.css">
    <!-- Mean Menu CSS -->
    <link rel="stylesheet" href="assets/css/meanmenu.css">
    <!-- Modal Video CSS -->
    <link rel="stylesheet" href="assets/css/modal-video.min.css">
    <!-- Flat Icons CSS -->
    <link rel="stylesheet" href="assets/fonts/flaticon.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!-- Popup Image CSS -->
    <link rel="stylesheet" href="assets/css/lightbox.min.css">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="assets/css/odometer.min.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="assets/css/responsive.css">

    <title>Findo - Fundraising and Charity HTML Template</title>

    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>
<body>
<!-- Preloader -->
<div class="loader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="pre-box-one">
                <div class="pre-box-two"></div>
            </div>
        </div>
    </div>
</div>
<!-- End Preloader -->

<!-- Header -->
<div class="header-area">
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <div class="left">
                    <ul>
                        <li>
                            <i class="icofont-location-pin"></i>
                            <a href="#">6B, Helvetica street, Jordan</a>
                        </li>
                        <li>
                            <i class="icofont-ui-call"></i>
                            <a href="tel:+0123456987">+0123-456-987</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="right">
                    <ul>
                        <li>
                            <span>Follow Us:</span>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="icofont-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="icofont-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="icofont-youtube-play"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="icofont-instagram"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="dropdown nav-flag-dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="assets/img/flag1.jpg" alt="Flag">
                            Eng
                            <i class="icofont-simple-down"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">
                                <img src="assets/img/flag2.jpg" alt="Flag">
                                Ger
                            </a>
                            <a class="dropdown-item" href="#">
                                <img src="assets/img/flag3.jpg" alt="Flag">
                                Isr
                            </a>
                            <a class="dropdown-item" href="#">
                                <img src="assets/img/flag4.jpg" alt="Flag">
                                USA
                            </a>
                        </div>
                    </div>
                    <div class="header-search">
                        <i id="search-btn" class="icofont-search-2"></i>
                        <div id="search-overlay" class="block">
                            <div class="centered">
                                <div id="search-box">
                                    <i id="close-btn" class="icofont-close"></i>
                                    <form>
                                        <input type="text" class="form-control" placeholder="Search..."/>
                                        <button type="submit" class="btn">Search</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Header -->

<!-- Navbar -->
<div class="navbar-area sticky-top">
    <!-- Menu For Mobile Device -->
    <div class="mobile-nav">
        <a href="index.html" class="logo">
            <img src="assets/img/logo-two.png" alt="Logo">
        </a>
    </div>

    <!-- Menu For Desktop Device -->
    <div class="main-nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="index.html">
                    <img src="assets/img/logo.png" alt="Logo">
                </a>
                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle active">Home <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="index.html" class="nav-link active">Home Demo One</a>
                                </li>
                                <li class="nav-item">
                                    <a href="index-2.html" class="nav-link">Home Demo Two</a>
                                </li>
                                <li class="nav-item">
                                    <a href="index-3.html" class="nav-link">Home Demo Three</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">Pages <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle">Users <i class="icofont-simple-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="sign-in.html" class="nav-link">Sign In</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="sign-up.html" class="nav-link">Sign Up</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="gallery.html" class="nav-link">Gallery</a>
                                </li>
                                <li class="nav-item">
                                    <a href="testimonials.html" class="nav-link">Testimonials</a>
                                </li>
                                <li class="nav-item">
                                    <a href="team.html" class="nav-link">Team</a>
                                </li>
                                <li class="nav-item">
                                    <a href="faq.html" class="nav-link">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a href="404.html" class="nav-link">404 Error Page</a>
                                </li>
                                <li class="nav-item">
                                    <a href="coming-soon.html" class="nav-link">Coming Soon</a>
                                </li>
                                <li class="nav-item">
                                    <a href="privacy-policy.html" class="nav-link">Privacy Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a href="terms-conditions.html" class="nav-link">Terms & Conditions</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">About <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="donations.html" class="nav-link">Donations</a>
                                </li>
                                <li class="nav-item">
                                    <a href="donation-details.html" class="nav-link">Donation Details</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">Donations <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="donations.html" class="nav-link">Donations</a>
                                </li>
                                <li class="nav-item">
                                    <a href="donation-details.html" class="nav-link">Donation Details</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">Events <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="events.html" class="nav-link">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a href="event-details.html" class="nav-link">Event Details</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link dropdown-toggle">Blog <i class="icofont-simple-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item">
                                    <a href="blog.html" class="nav-link">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a href="blog-details.html" class="nav-link">Blog Details</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="contact.html" class="nav-link">Contact</a>
                        </li>
                    </ul>
                    <div class="side-nav">
                        <a class="donate-btn" href="#">
                            Donate
                            <i class="icofont-heart-alt"></i>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- End Navbar -->
