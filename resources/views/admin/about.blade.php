@extends('adminlte::page')
@section('title', 'About')

@section('content')

    <div id="app">
        <about @if(isset($about)) :about="{{ $about }}" @endif> </about>
    </div>
    
 @stop
 <script src="{{ mix('js/app.js') }}" defer></script>