@extends('adminlte::page')
@section('title', 'Admin Donations')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <section class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1>Admin Donations</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                                    <li class="breadcrumb-item active">Admin Donations</li>
                                </ol>
                            </div>
                        </div>
                    </div><!-- /.container-fluid -->
                </section>
                @include('layouts.alert')

                <section class="content">

                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Donations</h3>
                        </div>
                        <div class="card-body p-0">
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 20%">
                                        Title
                                    </th>
                                    <th style="width: 40%">
                                        Description
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    <th style="width: 8%" class="text-center">
                                        Status
                                    </th>
                                    <th style="width: 20%">
                                        Manage
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($donations as $donation)
                                <tr>
                                    <td>
                                        <a>
                                            {{ $donation->title }}
                                        </a>
                                    </td>
                                    <td>
                                        {!! \Illuminate\Support\Str::limit($donation->description, 150, '...') !!}
                                    </td>
                                    <td class="">
                                        {{ $donation->category->title }}
                                    </td>
                                    <td class="project-state">
                                        <span class="badge {{ ($donation->active) ? 'badge-success' : 'badge-primary' }}">{{ ($donation->active) ? 'Active' : 'Inactive' }}</span>
                                    </td>
                                    <td class="p-0">
                                        <a class="btn btn-primary btn-sm" href="{{ route('donations.view', $donation->id) }}">
                                            <i class="fas fa-eye">
                                            </i>
                                            View
                                        </a>
                                        <a class="btn btn-info btn-sm" href="{{ route('donations.edit', $donation->id) }}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                            Edit
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="{{ route('donations.delete', $donation->id) }}">
                                            <i class="fas fa-trash">
                                            </i>
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </section>
            </div>
        </div>
    </div>
    @include('layouts.confirm_modal')
    @push('js')
        <script src="{{ asset('js/admin.js') }}"></script>
    @endpush
@stop
