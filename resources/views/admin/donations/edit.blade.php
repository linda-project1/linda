@extends('adminlte::page')
@section('title', $title)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>{{ strtoupper($title) }}</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('donations.index') }}">Donations</a></li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            @include('layouts.alert')

            <section class="content">

                <!-- Default box -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">{{ strtoupper($title) }}</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                {{{ Form::model($donation, ['route' => ['donations.update', $donation->id], 'enctype' => 'multipart/form-data', 'method' => 'POST']) }}}
                                    @csrf
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" name="title" value="{{ $donation->title }}" class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Enter Title" required>
                                                    @error('title')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Select Category</label>
                                                    {!! Form::select('category', $categories, $donation->category_id, ['placeholder' => 'Pick a category...', 'class' => 'form-control custom-select2', 'required']) !!}
                                                    @error('category')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="tags">Select Hash Tags</label>
                                                    {!! Form::select('tags[]', $tags, $donation->tags, ['class' => 'form-control custom-select2', 'multiple' => 'multiple']) !!}
                                                    @error('tags')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea rows="10" name="description" class="redactor @error('description') is-invalid @enderror" id="description" placeholder="Enter description here...!" required>{!! $donation->description !!}</textarea>
                                                    @error('description')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                @if($donation->image)
                                                    <div class="form-group">
                                                        <img src="{{ asset('storage/'.$donation->image->path) }}" width="100%" height="auto" alt="{{ $donation->title }}" />
                                                    </div>
                                                @endif
                                                <div class="form-group">
                                                    <label for="image">Donation Image</label>
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            <input name="image" type="file" accept="image/*" class="custom-file-input" id="image">
                                                            <label class="custom-file-label" for="image">Choose file</label>
                                                        </div>
                                                    </div>
                                                    <div class="text-red text-sm">Uploading a new image will replace the existing one if available!</div>
                                                    @error('image')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-6 pl-5">
                                                <div class="input-group">
                                                    <label for="active">Donation Status</label>
                                                </div>
                                                <div class="form-check">
                                                    <input type="checkbox" name="active" class="form-check-input" id="active" {{ $donation->active ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="active">Active</label>
                                                </div>
                                                @error('active')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary right">Update</button>
                                    </div>
                                {{{ Form::close() }}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->

            </section>
        </div>
    </div>
</div>
@push('js')
    <script src="{{ asset('js/admin.js') }}"></script>
@endpush
@stop
