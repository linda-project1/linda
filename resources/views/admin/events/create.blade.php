@extends('adminlte::page')
@section('title', $title)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>{{ strtoupper($title) }}</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('events.index') }}">Events</a></li>
                                <li class="breadcrumb-item active">{{ $title }}</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            @include('layouts.alert')

            <section class="content">

                <!-- Default box -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">{{ strtoupper($title) }}</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form method="POST" action="{{ route('events.save') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" placeholder="Enter Title" required>
                                                    @error('title')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="category">Select Category</label>
                                                    {!! Form::select('category', $categories, null, ['placeholder' => 'Pick a category..!', 'class' => 'form-control custom-select2', 'required']) !!}
                                                    @error('category')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea rows="10" name="description" class="redactor @error('description') is-invalid @enderror" id="description" placeholder="Enter description here...!" required></textarea>
                                                    @error('description')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
{{--                                            <div class="col-md-6">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label for="start_time">Start Time</label>--}}
{{--                                                    <input type="text" name="start_time" class="form-control datetimepicker @error('start_time') is-invalid @enderror" id="start_time" placeholder="Start Time" required>--}}
{{--                                                    @error('start_time')--}}
{{--                                                        <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                                                    @enderror--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div class='col-sm-6'>
                                                <div class="form-group">
                                                    <label for="start_time">Start Time</label>
                                                    <div class='input-group date datetimepicker' id='start_time'>
                                                        <input type='text' name="start_time" class="form-control" />
                                                        <span class="input-group-addon">
                                                           <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    @error('start_time')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="end_time">End Time</label>
                                                    <input type="text" name="end_time" class="form-control datetimepicker @error('end_time') is-invalid @enderror" id="end_time" placeholder="End Time" required>
                                                    @error('end_time')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="location">Location</label>
                                                    <textarea name="location" rows="5" class="redactor @error('location') is-invalid @enderror" id="location" placeholder="Event Location" required> </textarea>
                                                    @error('location')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="organizer">Organizer</label>
                                                    <input type="text" name="organizer" class="form-control @error('organizer') is-invalid @enderror" id="organizer" placeholder="Event Organizer" required>
                                                    @error('organizer')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact_number">Contact Number</label>
                                                    <input type="text" name="contact_number" class="form-control @error('contact_number') is-invalid @enderror" id="contact_number" placeholder="Event Contact Number" required>
                                                    @error('contact_number')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact_email">Event Email</label>
                                                    <input type="text" name="contact_email" class="form-control @error('contact_email') is-invalid @enderror" id="contact_email" placeholder="Event Contact Email" required>
                                                    @error('contact_email')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="image">Event Image</label>
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            <input name="image" type="file" accept="image/*" class="custom-file-input" id="image">
                                                            <label class="custom-file-label" for="image">Choose file</label>
                                                        </div>
                                                    </div>
                                                    @error('image')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-6 pl-5">
                                                <div class="input-group">
                                                    <label for="active">Event Status</label>
                                                </div>
                                                <div class="form-check">
                                                    <input type="checkbox" name="active" class="form-check-input" id="active" checked>
                                                    <label class="form-check-label" for="status">Active</label>
                                                </div>
                                                @error('active')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary right">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->

            </section>
        </div>
    </div>
</div>
@push('js')
    <script src="{{ asset('js/admin.js') }}"></script>
@endpush
@stop
