# DANHIKO PROJECT
Better Place Foundation - Website.

## Table of Contents

- [Features](#features)
- [System Requirements](#system-requirements)
- [Links](#links)
- [Installation](#installation)
- [Software Used](#software-used)
- [Bugs and Feedback](#bugs-and-feedback)

## Features
- Website for Danhiko Project

## System Requirements
- PHP > 7.2
- PHP Extensions: PDO, cURL, Bcmath, Mcrypt, XML, GD
- Node.js > 6.0
- Composer > 2.0.0
- Laravel > 6.x
- MySql > 8.0
- Apache > 2.4

## Software Used
- Laravel
- Bootstrap
- jQuery
- VueJs
- CSS

## Links

**[Staging Site](https://betterplace.besthandyman.co.za)**

**[Developer FB Page](https://facebook.com/mugotech)**

**[Developer Website](https://vakara.africa)**


## Installation

Clone repository

`$ git clone git clone git@bitbucket.org:linda-project1/linda.git betterplace`

Change into the working directory

`$ cd betterplace`

Copy `.env.example` to `.env` and modify `.env` values according to your environment

`$ cp .env.example .env`

Install composer dependencies

`$ composer install --prefer-dist`

Install node dependencies

`$ npm install`

Change folder permissions

`$ sudo chown -R www-data:www-data storage && sudo chown -R www-data:www-data vendor && sudo chown -R www-data:www-data public && sudo chown -R www-data:www-data node_modules && sudo chown -R www-data:www-data bootstrap/cache && sudo chmod -R 775 storage bootstrap/cache && sudo chmod -R 775 storage public`

An application key can be generated with the command

`$ php artisan key:generate`

Run these commands to create the tables within the defined database and populate seed data

`$ php artisan migrate:refresh --seed`

Compile node dependencies depending app env

`$ npm run dev` when app is in development Or `$ npm run prod` when app is in production.

## Security Vulnerabilities and Feedback
If you discover a security vulnerability within [Better Place Foundation](https://bitbucket.org/linda-project1/linda), please send an e-mail to the following developers:
- Tera Fumba via: [terafumba@gmail.com](mailto:terafumba@gmail.com).
- Mugo Machaka via: [juniormachaka@gmail.com](mailto:juniormachaka@gmail.com)

All security vulnerabilities will be promptly addressed.

## License
The [Better Place Foundation](https://bitbucket.org/linda-project1/linda) is an open-source software built on [Laravel Framework](https://github.com/laravel). Any developers are free to use for personal use and or contribute if you have passion in providing support to Charity Web systems. Better Place Foundation is licensed under the [MIT license](https://opensource.org/licenses/MIT).
