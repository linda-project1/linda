<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        $categories = [
            'Medical', 'Education', 'Family', 'Funding', 'Drought', 'Family', 'Fire'
        ];

        foreach ($categories as $key => $category){
            DB::table('categories')->updateOrInsert(
                [
                    'id' => $key + 1
                ],
                [
                    'title' => $category,
                    'description' => $category . ' Category short description..!',
                    'created_at' => $now,
                    'updated_at' => $now
                ]
            );
        }

    }
}
