<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        $tags = [
            'Medical', 'Donation', 'Food', 'Help', 'Fees', 'Covid 19', 'Funding'
        ];

        foreach ($tags as $key => $tag){
            DB::table('tags')->updateOrInsert(
                [
                    'id' => $key + 1
                ],
                [
                    'title' => $tag,
                    'description' => $tag . ' Tag short description..!',
                    'created_at' => $now,
                    'updated_at' => $now
                ]
            );
        }
    }
}
