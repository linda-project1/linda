<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $now = Carbon::now();

        DB::table('users')->updateOrInsert(
            [
                'id' => 1
            ],
            [
                'name' => 'Mugove',
                'surname' => 'Machaka',
                'email' => 'mugopower@gmail.com',
                'cell_number' => '0123456789',
                'password' => Hash::make('12345678'),
                'user_type_id' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ]
        );
    }
}
