<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return redirect('/login');
//});

Auth::routes();
//AboutUsController
//Route::get('/home', 'HomeController@index')->name('home');
///get_about


Route::get('/', function() { return view('interface.index'); });



/**
 * Admin Middleware and Route grouping
 */
Route::middleware(['auth', 'admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        /** Admin Donations Routes */
        Route::get('donations', 'DonationController@index')->name('donations.index');
        Route::get('donations/view/{id}', 'DonationController@view')->name('donations.view');
        Route::get('donations/create', 'DonationController@create')->name('donations.create');
        Route::post('donations/save', 'DonationController@save')->name('donations.save');
        Route::get('donations/edit/{id}', 'DonationController@edit')->name('donations.edit');
        Route::post('donations/update/{id}', 'DonationController@update')->name('donations.update');
        Route::get('donations/delete/{id}', 'DonationController@delete')->name('donations.delete');

        /** Admin Events Routes */
        Route::get('events', 'EventController@index')->name('events.index');
        Route::get('events/view/{id}', 'EventController@view')->name('events.view');
        Route::get('events/create', 'EventController@create')->name('events.create');
        Route::post('events/save', 'EventController@save')->name('events.save');
        Route::get('events/edit/{id}', 'EventController@edit')->name('events.edit');
        Route::post('events/update/{id}', 'EventController@update')->name('events.update');
        Route::get('events/delete/{id}', 'EventController@delete')->name('events.delete');

        Route::get('home', 'AdminController@index')->name('admin.home');
        Route::get('contact', 'ContactController@index')->name('contact.index');

        //Route::get('/about-us', 'AboutUsController@index')->name('about-us');
        Route::get('/about-us', 'AboutUsController@getView')->name('admin-about-us');
        Route::get('/get_about', 'AboutUsController@getAbout');
        Route::get('/about_can_edit', 'AboutUsController@canEdit');
        Route::post('/save-volunteers', 'AboutUsController@saveVolunteers')->name('save-volunteers');
        Route::put('/save-volunteers', 'AboutUsController@updateVolunteers');
        Route::post('/save-features', 'AboutUsController@saveFeatures')->name('save-features');
        Route::post('/save-dreams', 'AboutUsController@saveDreams')->name('save-dreams');
        Route::post('/save-about', 'AboutUsController@saveAbout')->name('save-about');
        Route::post('/update-banner', 'AboutUsController@updateBanner');
        Route::post('/update-maving-image', 'AboutUsController@updateImg');


        Route::post('/update-team-banner', 'TeamsController@saveTeamBanner');
        Route::post('/save_team_members', 'TeamsController@saveTeamMembers');
        Route::get('/get_team_banner', 'TeamsController@getTeamInfo');
        Route::get('/admin-team', 'TeamsController@index');
    });
});


/**
 * Routes for public Users
 */
//Route::get('/', 'HomeController@index')->name('home');
